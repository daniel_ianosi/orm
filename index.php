<?php
include "ORM/BaseEntity.php";
include "ORM/Utils.php";
include "ORM/EntityManager.php";
include "ORM/Repository.php";
include "Entities/BlogPost.php";
include "Entities/Author.php";

$blogPost = new BlogPost();

$blogPost->setTitle('First post');
$blogPost->setContent('First content');

$author = new Author();
$blogPost->setAuthor($author);

$em = EntityManager::getInstance();
$em->persist($blogPost);
$em->flush();

$blogPost->setTitle('Second Title');
$em->persist($blogPost);
$em->flush();

$em->remove($blogPost);
$em->flush();

var_dump($blogPost);

/** @var BlogPost $blogPost2 */
$blogPost2 = $em->getRepository(BlogPost::class)->find(4);
var_dump($blogPost2);

var_dump($blogPost2->getAuthor());
var_dump($blogPost2);
var_dump($blogPost2->getAuthor()->getBlogPosts());

var_dump($blogPost2);

