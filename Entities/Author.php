<?php

/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 1/23/2019
 * Time: 7:36 PM
 */
class Author extends BaseEntity
{
    /** @var  string */
    public $name;

    /** @var  BlogPost[] */
    public $blogPosts;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Author
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return BlogPost[]
     */
    public function getBlogPosts()
    {
        return $this->blogPosts;
    }

    /**
     * @param BlogPost[] $blogPosts
     * @return Author
     */
    public function setBlogPosts($blogPosts)
    {
        $this->blogPosts = $blogPosts;
        return $this;
    }


    /**
     * @return []
     */
    public function getRelations()
    {
        return [
            'blogPosts' => [
                'type'=>'OneToMany',
                'entity'=>'BlogPost',
                'column'=>'author_id'
            ]
        ];
    }
}