<?php

/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 1/21/2019
 * Time: 8:07 PM
 */
abstract class BaseEntity
{
    public $id;

    /**
     * BaseEntity constructor.
     */
    public function __construct()
    {
        foreach ($this->getRelations() as $attribute=>$relationData){
            unset($this->$attribute);
        }
    }

    public function __get($attribute)
    {
        $relation = $this->getRelations()[$attribute];

        $em=EntityManager::getInstance();
        $type = $relation['type'];
        $entity = $relation['entity'];
        $column = $relation['column'];

        if ($type=='ManyToOne'){
            $getter = 'get'.Utils::fromUnderscore($column, true);
            $this->$attribute = $em->getRepository($entity)->find($this->$getter());
        } else {
            $this->$attribute = $em->getRepository($entity)->findBy([$column=>$this->getId()]);
        }

        return $this->$attribute;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return BaseEntity
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return []
     */
    public abstract function getRelations();


}