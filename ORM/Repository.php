<?php

/**
 * Created by PhpStorm.
 * User: daniel-laptop
 * Date: 1/21/2019
 * Time: 9:15 PM
 */
class Repository
{
    /** @var  string */
    public $entityName;

    /** @var  EntityManager */
    public $entityManager;

    /**
     * Repository constructor.
     * @param $entityName
     */
    public function __construct($entityName,EntityManager $entityManager)
    {
        $this->entityName = $entityName;
        $this->entityManager = $entityManager;
    }


    /**
     * @param $criteria
     * @return BaseEntity[]
     */
    public function findBy($criteria){
        return $this->entityManager->getData($this->entityName, $criteria);
    }

    /**
     * @param $criteria
     * @return BaseEntity
     */
    public function findOneBy($criteria){
        return $this->entityManager->getData($this->entityName, $criteria, true);

    }

    /**
     * @param $id
     * @return BaseEntity
     */
    public function find($id){
        return $this->findOneBy(['id'=>$id]);
    }
}